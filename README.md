# Decent

![Bob Ross & Bubbles][decent]


## Overview

Decent aims to provide a general-purpose content-centric overlay network
enabling network peers to publish content and services. Decent itself won't
directly implement content or services to be used by end-users, rather it will
comprise a suite of tools enabling technical users of the network to design
and publish their own content and services.

Broadly, Decent has the following goals:

* **Decentralization**: the network has no single point of failure
* **Homogeneity**: all peers of the network are uniform in class
* **Resilliance and Censorship-Resistance**: content and services are
  replicated across all "interested" peers
* **Crypto**: Decent content may be encrypted and/or signed depending on the
  application, providing content secrecy and/or authentication
* **Extensibility**: technical users may use the fundamental tools exposed by
  Decent to create and publish novel decentralized content and services

## Content and Services

Services are replicated communication and data storage systems with
programmable business logic (e.g., access control, content requirements).
Content is stuff that is shared via services.

Generally, Decent lends itself to publishing content that is:

* append-only (or append-modifiable [e.g., through diffs])
* small-to-medium in total size (0-100GB for a single collection)
* permanent (Decent will make no attempt to enable revocation or deletion of
  content from the network after publishing).

### Example Services

* blogging, micro-blogging, photo-sharing, etc
* public or private social networking
* encrypted group chat
* document editing
* video live-streaming
* news aggregators
* message boards
* email

## How it Works

### Topic Block Trees (TBTs)

Services on the network exist as trees of data blocks, with distinctly rooted
trees comprising distinct topics. Topics provide isolation between instances of
services. Topics may be modified only by appending new blocks onto existing
blocks (via referential linkage). Once appended to a topic block tree, all
blocks are immutable.

TBTs are replicated between "interested" users, with interest being an
expression of desire to participate in a service. For example, in a
microblogging service, the complete history of messages (aka tweets) are
encapsulated in a single TBT which is completely replicated by all users
wishing to receive the microblog, but also by the publisher of that service.
Users who access a service also form the distribution network, in an
arrangement similar to BitTorrent swarms, enabling network throughput capacity
to automatically grow with service interest.

The rules describing whether, and how, users may interact with TBTs are what
define and distinguish services. When a TBT instance is created, it is
associated with a predicate program which determines (at minimum) whether a
given user may append a given block of content to the TBT. In the microblogging
example, the TBT predicate would accept any content appended to the TBT only if
the content was created by the publisher (i.e., the user who created the TBT).
TBT predicates define the **type** of a service instance.  Any user with the
required technical expertise may write a new TBT predicate, thus defining a new
type of service. TBT predicates may be amended after creation by appending a
revised TBT predicate (under the condition that the block in which the amended
TBT predicate exists is accepted by the existing TBT predicate).

Individual TBTs are identified by a hash of the origin node (i.e., the TBT's
first node, or the node whose predecessor reference link is undefined). The
hash function maps the origin node into a space of sufficiently high
dimensionality as to ensure universal uniqueness.

### Users

Decent users interact with the network using public key cryptography. A user
generates a keypair:

* A hash of the public key forms a universally unique identifier for the user
* The private key enables signatures for authentication, and deciphering
  asymmetrically encrypted messages
* The public key enables third parties to authenticate the user and send
  encrypted messages

### Discovery

How can users discover services or other users? In both cases a distributed
hash table is used. Participants in the Decent network are also peers in the
DHT used to resolve services and other users. In both cases a key is selected
which represents the service or user, respectively, which maps to a set of
network nodes which can provide more information about the subject in question.

#### Of Services

Because service instances are also TBTs, service instances may be uniquely
identified as such (i.e., using a hash of the TBT's origin node). As such, the
ID of the service (and thus the TBT) becomes the key into the DHT.

The payload mapped onto from the key is a set of known replicants of the
service TBT. Each replicant is described by IP connection parameters (e.g.,
`udp://hostname:port`).

#### Of Other Users

*Work in progress*

Users publish a specific type of service which describes a number of properties
of the user, some or all of which would be encrypted with the public keys of
the user's trusted "friends":

* IP connection details for direct P2P communication with the user.
* Identity information (e.g. avatar image, real name, phone number)
* Services recommended by or in which the user participates
* Key changes or revocations

We've already seen that users can be uniquely identified by the hash of their
public keys, which will also serve as a DHT key.

As in the case for general services, the payload mapped onto from the key is a
set of known replicants of the user TBT. Each replicant is described by IP
connection parameters (e.g., `udp://hostname:port`).

### TBT Swarms
*TODO*

### Peer Duplication
*TODO*

### Service Frontends
*TODO*

### P2P Facilitation
*TODO*

*E.g., UPnP, STUN, TURN*

### Proxies
*TODO*




[decent]: decent.jpg
